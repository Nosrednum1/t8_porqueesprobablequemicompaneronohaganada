* #Estudiante A#
> _**Anderson Barragán Agudelo 201719821**_
* **Estudiante B**
> nombre estudiante B ~~aunque no creo que mi compañero si quiera mire el repositorio~~

## Creación de métodos por estudiante:##

Anderson Barragán | Compañero
------------- | -------------
Requerimiento 1: carga| nada
Requerimiento 2: creación del grafo| nada
Requerimiento 3: carga para os vriados archivos| nada
Requerimiento 4: definir JSON para guardado del grafo| nada
Requerimiento 5: guardar y recargar el JSON del punto 4| nada

* Metodo 1

    Primera parte:

        Tomar la latitud y la longitud del punto de inicio del servicio (Lati, Logi)
        ▪ Verificar si en el grafo existe ya existe un vértice (vi), representado por una latitud y
            longitud de referencia (Latref, Logref), tal que el punto inicial del servicio (Lati, Logi)
            está a una distancia menor o igual a Dx a la redonda del punto (Latref, Logref)
            (utilizando la distancia harvesiana).
        • Si el punto (Lati, Logi) estuviera a una distancia menor o igual a Dx de varios
            vértices vi definidos, seleccionar el vértice de referencia vi más cercano.
        • Si estuviera a la misma distancia de varios vértices vi definidos, escoger solo
            uno de ellos de forma aleatoria.
        • Adicionar el identificador del servicio que se está procesando al vértice vi seleccionado.
        • Si en el grafo no existe un vértice representado por una latitud y longitud (Latref,
            Logref), tal que la distancia harvesiana del punto (Lati, Logi) es menor o igual a Dx a
            la redonda, se debe crear un nuevo vértice (vi) en el grafo y utilizar las coordenadas
            de inicio (Lati, Logi) como el punto de referencia del nuevo vértice vi

    Segunda parte:
    
        Tomar la latitud y longitud del punto final del servicio (Latf, Logf) y repetir los mismos
        pasos seguidos para el punto de inicio identificando un vértice existente vf que contenga a
        (Latf, Lonf) a una distancia menor o igual a Dx o crear un nuevo vértice vf en el grafo. 
        

* Blue
        
        > This is the first level of quoting.
        >
        > > This is nested blockquote.
        >
        > Back to the first level.
* Red