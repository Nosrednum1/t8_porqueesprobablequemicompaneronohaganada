package model.data_structures;

import java.util.Iterator;

public class Bag <Item> implements Iterable<Item>{

	private Node<Item> first;
	private int size;

	public Bag() {first = null; size = 0;}

	public int size() {return size;}

	public void add(Item item) {
		Node<Item> second = first;
		first = new Node<>();
		first.item = item;
		first.Next = second;
		size++;
	}

	public boolean isEmpty() {return first != null;	}

	@Override public Iterator<Item> iterator() { return new myIterator<>(first); }

	private class Node<Item>{  private Item item;	private Node<Item> Next;}

	private class myIterator<Item> implements Iterator<Item>{
		private Node<Item> actual;
		public myIterator(Node<Item> inicial) {this.actual = inicial;}

		@Override public boolean hasNext() {return actual != null;}
		@Override public Item next() {Item toRet = actual.item; actual = actual.Next; return toRet;}
	}

}
