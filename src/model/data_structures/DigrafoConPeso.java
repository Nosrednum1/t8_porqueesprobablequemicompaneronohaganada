package model.data_structures;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import model.value_objects.Servicio;

public class DigrafoConPeso {

	/**
	 * lista de adjacencia con un vertice y lista de enlaces en cada espacio del arreglo
	 */
	private SeparateChainingHashST<String, Vertex> adjacentList;

	/**
	 * crea un nuevo D�grafo con espacio para <b>capacity</b> vertices
	 * @param capacity cantidad de espacios para la isersi�n de v�rtices
	 */
	public DigrafoConPeso(int capacity) {
		adjacentList = new SeparateChainingHashST<>(capacity);
	}

	public DigrafoConPeso(String ruta) {
		File f = new File(ruta);
		String name = ruta;
		String[] vertices = name.split("-");
		int capacity = Integer.parseInt(vertices[1]);
		adjacentList = new SeparateChainingHashST<>(capacity);
		Gson g = new GsonBuilder().create();
		FileInputStream stream;
		try {
			stream = new FileInputStream(f);
			JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8")); reader.beginArray();
			while (reader.hasNext()) {
				Edge e = g.fromJson(reader, Edge.class);
				addEdge(e);}
		} catch (Exception e) {e.printStackTrace();System.err.println("Error en la carga del d�grafo a partir del archivo");}
	}

	public int edges() {int toRet = 0;for (Vertex v : adjacentList.vals())toRet += v.degree();return toRet;}
	public int vertices() {return adjacentList.size();}
	public boolean isEmpty() {return adjacentList.size() == 0;}

	public void addVertice(String infoVertice) {
		if(adjacentList.contains(infoVertice))return;
		adjacentList.put(infoVertice, new Vertex(infoVertice));
	}
	public String getInfoVertice(String infoVertice) {
		Vertex temp = adjacentList.get(infoVertice);return (temp != null)?temp.info():"NaN";}

	/**
	 * a�ade un vertice ya creado
	 */
	public void addEdge(Edge newEdge) {
		if(!adjacentList.contains(newEdge.from()))
			adjacentList.put(newEdge.from(), new Vertex(newEdge.from()));

		if(!adjacentList.contains(newEdge.to()))
			adjacentList.put(newEdge.to(), new Vertex(newEdge.to()));
		adjacentList.get(newEdge.from()).addEdge(newEdge);
		adjacentList.get(newEdge.to()).addEdgeIn(newEdge);
	}
	/**
	 * crea y a�ade un vertice
	 */
	public void addEdge(String idInicio, String idFinal, Servicio infoArc) {
		if(!adjacentList.contains(idInicio))
			adjacentList.put(idInicio, new Vertex(idInicio));

		if(!adjacentList.contains(idFinal))
			adjacentList.put(idFinal, new Vertex(idFinal));

		Edge newEdge = new Edge(idInicio, idFinal);
		newEdge.addService(infoArc);
		boolean existe = adjacentList.get(idInicio).addEdge(newEdge);
		if(!existe)adjacentList.get(idFinal).addEdgeIn(newEdge);
	}

	public void setInfoArc(String idInicio, String idFinal, Servicio infoArc) {
		adjacentList.get(idInicio).setInfoEdge(idFinal, infoArc);
	}

	public Iterable<Edge> adyacentesA(String vertice) {return adjacentList.get(vertice).edges();}
	public Iterable<Vertex> allVertices(){return this.adjacentList.vals();}
}
