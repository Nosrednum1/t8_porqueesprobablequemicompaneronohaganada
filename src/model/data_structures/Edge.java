package model.data_structures;

import model.value_objects.Servicio;

public class Edge implements Comparable<Edge>{

	private double total, totalMiles;
	private int srvcWToll, cantServices, totalSeconds;
	private String ladoIn, ladoFn;

	public Edge(String Inicio, String Fin) {
		this.ladoIn = Inicio;
		this.ladoFn  = Fin ;
		total = totalMiles = srvcWToll = cantServices = totalSeconds = 0;
	}

	public String from() 	{return ladoIn;}
	public String to()  	{return ladoFn;}
	public double weight() 	{return (total + totalMiles + totalSeconds)/(cantServices -srvcWToll);}

	public void addService(Servicio s) {
		cantServices++;
		if(s.payTolls())srvcWToll++;
		total 			+= s.getTripTotal( ) ;
		totalMiles 		+= s.getTripMiles( ) ;
		totalSeconds 	+= s.getTripSeconds();
	}

	public void sumWeight(Edge weightToAdd) {
		this.total 		  += weightToAdd.total		 ;
		this.totalMiles   += weightToAdd.totalMiles	 ;
		this.srvcWToll 	  += weightToAdd.srvcWToll	 ;
		this.cantServices += weightToAdd.cantServices;
		this.totalSeconds += weightToAdd.totalSeconds;
	}

	public boolean equals(Edge e) {return this.compareTo(e) ==0;}
	@Override public int compareTo(Edge e) {return (this.ladoIn +";;;"+this.ladoFn).compareTo((e.ladoIn+";;;"+e.ladoFn));}
	@Override public String toString(){ return String.format("%d-%d %.2f", ladoIn, ladoFn,weight());}
}
