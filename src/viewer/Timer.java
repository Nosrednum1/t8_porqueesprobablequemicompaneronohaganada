package viewer;

public class Timer {

	private long time_start;
	private long time_end;
	private long memoryBeforeCase1;

	public void start() {
		memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		time_start = System.nanoTime();
	}

	public void end() {
		time_end = System.nanoTime();
		long total = (time_end - time_start)/(1000000);
		long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Tiempo en cargar: " + total + " milisegundos \nMemoria utilizada: "
				+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");
	}
}
