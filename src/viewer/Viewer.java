package viewer;

import java.util.Scanner;

import model.logic.taller8Manager;

public class Viewer {
	private static final Timer t = new Timer();
	private static final taller8Manager manager = new taller8Manager();

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		while(!fin) {	printMenu();			
		int opcion = sc.nextInt();
		switch (opcion) {
		case 1:	
			printMenuLoad();
			int optionCargar = sc.nextInt();
			//
			String linkJson = "";
			switch (optionCargar){
			case 1:	linkJson = manager.DIRECCION_SMALL_JSON  ;	break; //Direcci�n del archivo de datos peque�o
			case 2:	linkJson = manager.DIRECCION_MEDIUM_JSON ;	break; //Direcci�n del archivo de datos mediano
			case 3:	linkJson = manager.DIRECCION_LARGE_JSON  ;	break; //Direcci�n del archivo de datos grande
			case 4:	linkJson = manager.DIRECCION_TEST_JSON   ;	break;} // Test con pocos taxis pero varios casos
			System.out.println("Datos cargados: " + linkJson);
			t.start();
			manager.load(linkJson);
			t.end();	break;
		case 2:	
			manager.saveGraph();
			break;
		case 3:	
			manager.loadTextArchive();
			break;

		case 4:	
			System.out.println("Por favor introduzca el numero de archivo que carg� para \n comparar la carga del grafo a partir del archivo");
			String nombreDeCarga = sc.next();
			if(nombreDeCarga.equals("1"))nombreDeCarga = "./docs/smallGraph25-123-.json";
			else if(nombreDeCarga.equals("2"))nombreDeCarga = "./docs/mediumGraph25-222-.json";
			else if(nombreDeCarga.equals("3"))nombreDeCarga = "./docs/largeGraph25-321-.json";
			else if(nombreDeCarga.equals("4"))nombreDeCarga = "./docs/testGraph25-9-.json";
			manager.loadGraph(nombreDeCarga);
			break;
		case 5:	 fin = true; sc.close(); break; //m�todo de salida
		}}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 8----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println(" \n1. Cargar toda la informacion del sistema de una fuente de datos (small, medium o large).");

		System.out.println("\n Requerimientos (�tiles despues de la carga):\n");
		System.out.println(" 2. -guarda el grafo en un archivo JSON");
		System.out.println(" 3. -Actualiza la informaci�n del Leame.txt");
	}
	private static void printMenuLoad() {
		System.out.println("-- �Que fuente de datos desea cargar? JWL 16D");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- 4. Test");
		System.out.println("-- Ingrese el numero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
	}
}
